<h1> <b> Auto Submit </b> </h1>

This script automaticly creates a new repository, commit .gitignore to master,
create the dev branch and commit the given files into it, adds eran to the project
and submits a merge request.
to submit an excerise, just run this script and submit.

You can use auto-sumbit through the config file or through the CLI.

**Through the CLI arguments:**
    Usage: auto-submit.py auth_key project_name script_paths
    Where the arguments are the same as in the config file.
    
    
**Through the config file:**
fill the dictionary with your details:
*     'auth_key': Your private access token, can be generated in settings > access tokens.
*     'project_name': The name to give your repository.
*     'scripts': A list of the paths to all the files to submit.




**Through the prompt:**
    You will be prompted to fill in the details about the project by the script.

    
*Note that the .gitignore is automaticlly added and doesnt need to be in the script
paths.

**Note if the code fails after creating the repository (commit failed, file
not found etc, the repository will be deleted automtically)