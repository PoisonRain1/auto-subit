"""
Name: auto-submit.py
Purpose: Automatically create the needed things for gitlab.
Author: PoisonRain.
Change Log:
    22/4/20 - Created
"""

import gitlab
import os.path
import sys


def create_commit(project, branch, commit_message, files):
    """
    Commit to the gitlab project.
    :param gitlab.project project: The project instance to commit to.
    :param str branch: The branch to commit to.
    :param str commit_message: The commit message.
    :param list files: A list containing all the paths to the files to commit.
    :return: The commit instance returned from the api.
    """

    actions = [{'action': 'create', 'file_path': os.path.basename(file), 'content': open(file).read()} \
               for file in files]
    data = {
        'branch': branch,
        'commit_message': commit_message,
        'actions': actions
    }

    return project.commits.create(data)


def add_member(gl, project, username, rank=gitlab.MAINTAINER_ACCESS):
    """
    Add a member to the project, defaults to maintainer.
    :param gl: The gitlab instance.
    :param project: The current project
    :param str username: The user to add to the project.
    :param rank: The rank to apply, defaults to maintainer.
    :return: The added member.
    """

    user = gl.users.list(username=username)[0]
    return project.members.create({'user_id': user.id, 'access_level':
        rank})


def get_project_details():
    """
    ClI interface to get the project details from the user.
    :return: The details required to submit a project.
    """

    auth_key = input("Please enter your access token: ")
    project_name = input("Please enter your project name: ")
    script = ''
    scripts = []
    while script.lower() != 'stop':
        script = input("Enter a file path to upload (enter stop to finish): ")
        if script.lower() != 'stop':
            scripts.append(script)

    return auth_key, project_name, scripts


def get_user_arguments():
    if len(sys.argv) < 4:
        input_type = input("Please enter an input type: Prompt or Config").lower()

        if input_type == 'config':
            try:
                from config import arguments
                auth_key = arguments['auth_key']
                project_name = arguments['project_name']
                scripts = arguments['scripts']

            except (ImportError, KeyError):
                print("config file failed, please enter manually.")
                return get_project_details()

        else:
            return get_project_details()

    else:
        auth_key = sys.argv[1]
        project_name = sys.argv[2]
        scripts = sys.argv[3:]

    return auth_key, project_name, scripts


def main():
    auth_key, project_name, scripts = get_user_arguments()

    print("Authenticating...")
    gl = gitlab.Gitlab("https://gitlab.com/", auth_key)

    print("Creating repository...")
    project = gl.projects.create({'name': project_name})

    try:
        print("Initial commit...")
        create_commit(project, 'master', 'Initial commit', ['.gitignore'])

        branch = project.branches.create({'branch': 'dev',
                                          'ref': 'master'})

        print("Commiting scripts into dev...")
        commit = create_commit(project, 'dev', f'added {os.path.basename(scripts[0])}', scripts)

        print("Adding eran and creating merge request...")
        add_member(gl, project, 'bistitan')

        mr = project.mergerequests.create({'source_branch': 'dev',
                                           'target_branch': 'master',
                                           'title': project_name,
                                           'description': commit.title})

        print("Done!")

    except Exception as e:
        print(e)
        print("Failed to commit changes, deleting the repository")
        project.delete()


if __name__ == '__main__':
    main()
